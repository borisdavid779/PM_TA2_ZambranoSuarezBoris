package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // INICIO - CODE6

        // ACCEDE AL PARAMETRO 3.1
        String object_id = getIntent().getStringExtra("object_id");

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");
        //Se accede a los datos de la Database
        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id , new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e == null) {

                    //Se esta buscando los recursos, dentro de los cuales estan las ID
                    ImageView img = (ImageView) findViewById(R.id.thumbnail);
                    //Se esta añadiendo la imagen para mostrar en la activity
                    img.setImageBitmap((Bitmap)object.get("image"));

                    //Se esta buscando los recursos, dentro de los cuales estan las ID
                    TextView text1 = (TextView)findViewById(R.id.Text2);
                    text1.setText((String)object.get("name"));

                    //Se busca el recuerso y se especifica cual estamos buscando
                    TextView text2 = (TextView)findViewById(R.id.Text3);
                    //se pasa el dato mas el unicode de el signo dolar
                    text2.setText((String)object.get("price") + " \u0024");

                    TextView text4 = (TextView)findViewById(R.id.Text4);
                    text4.setText((String)object.get("description"));

                } else {
                // Error
                }
            }
        });
        // FIN - CODE6

    }

}
